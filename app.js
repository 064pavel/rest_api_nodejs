const express = require('express')
const router = require('./routes/api.routes')
const app = express()

app.use(express.json())
app.use('/api',router)

app.get('/', (req, res) => {
    res.send('working...')
})

app.listen(3000, ()=>{
    console.log('Server started');
})