const DB = require('../../database/database')

class PostController {

    async createPost(req, res) {
        try {
            const {title, text, user_id} = req.body;

            const userExists = await DB.query(`SELECT EXISTS(SELECT 1
                                                             FROM users
                                                             WHERE id = $1)`, [user_id]);

            if (!userExists.rows[0].exists) {
                return res.status(404).json('User not found');
            }

            const post = await DB.query(`INSERT INTO posts (title, text, user_id)
                                         VALUES ($1, $2, $3) RETURNING id`, [title, text, user_id]);

            res.json(post.rows[0].id);
        } catch (e) {
            console.error(e);
            res.status(500).json('Server error');
        }
    }


    async getPosts(req, res) {
        try {
            const posts = await DB.query(`SELECT p.*, u.username, u.email
                                          FROM posts p
                                                   JOIN users u ON p.user_id = u.id`);

            res.json(posts.rows);
        } catch (e) {
            console.error(e);
            res.status(500).json('Server error');
        }
    }

    async getPost(req, res) {
        try {
            const id = req.params.id
            const post = await DB.query(`SELECT p.*, u.username, u.email
                                         FROM posts p
                                                  JOIN users u ON p.user_id = u.id
                                         WHERE user_id = $1`, [id])
            res.json(post.rows[0])
        } catch (e) {
            console.error(e)
            res.status(500).json('server error')
        }
    }

    async updatePost(req, res) {
        try {
            const {id, title, text, user_id} = req.body

            const post = await DB.query(
                `UPDATE posts
                 SET title=$1,
                     text=$2,
                     user_id=$3
                 WHERE id = $4`,
                [title, text, user_id, id]
            )

            res.json(post.rows[0])

        } catch (e) {
            console.error(e)
            res.status(500).json('server error')
        }
    }

    async deletePost(req, res) {
        try {
            const id = req.params.id
            const post = await DB.query(
                `DELETE
                 FROM posts
                 WHERE id = $1`,
                [id]
            )
            res.json(post.rows[0])
        }catch (e) {
            console.error(e)
            res.status(500).json('server error')
        }
    }

}

module.exports = new PostController()