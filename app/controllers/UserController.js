const DB = require('../../database/database')

class UserController {

    async createUser(req, res) {
        try {
            const {username, email} = req.body;

            const user = await DB.query(
                `INSERT INTO users (username, email)
                 VALUES ($1, $2) RETURNING id`,
                [username, email]
            )

            res.json(user.rows[0].id);
        } catch (e) {
            console.error(e)
            res.status(500).json('server error');
        }
    }


    async getUsers(req, res) {
        try {

            const users = await DB.query(
                'SELECT * FROM users'
            )

            res.json(users.rows)
        } catch (e) {
            console.error(e)
            res.status(500).json('server error')
        }
    }

    async getUser(req, res) {

        try {
            const id = req.params.id
            const user = await DB.query(
                `SELECT *
                 FROM users
                 WHERE id = $1`, [id]
            )
            res.json(user.rows[0])
        } catch (e) {
            console.error(e)
            res.status(500).json('server error')
        }
    }

    async updateUser(req, res) {
        try {
            const {id, username, email} = req.body
            const user = await DB.query(
                `UPDATE users
                 SET username=$1,
                     email=$2
                 WHERE id = $3`,
                [username, email, id]
            )
            res.json(user.rows[0])
        } catch (e) {
            console.error(e)
            res.status(500).json('server error')
        }
    }

    async deleteUser(req, res) {
        try {
            const id = req.params.id
            const user = await DB.query(
                `DELETE
                 FROM users
                 WHERE id = $1`,
                [id])
            res.json(user.rows[0])
        } catch (e) {
            console.error(e)
            res.status(500).json('server error')
        }
    }

}

module.exports = new UserController()