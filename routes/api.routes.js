const Router = require('express')
const UserController = require('../app/controllers/UserController')
const PostController = require('../app/controllers/PostController')

const router = new Router

router.post('/users', UserController.createUser)
router.get('/users', UserController.getUsers)
router.get('/users/:id', UserController.getUser)
router.patch('/users', UserController.updateUser)
router.delete('/users/:id', UserController.deleteUser)

router.post('/posts', PostController.createPost)
router.get('/posts', PostController.getPosts)
router.get('/posts/:id', PostController.getPost)
router.patch('/posts', PostController.updatePost)
router.delete('/posts/:id', PostController.deletePost)

module.exports = router